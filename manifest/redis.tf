# https://www.terraform.io/docs/providers/aws/r/elasticache_cluster.html

resource "aws_elasticache_cluster" "redis" {
  cluster_id           = "gnar-cache"
  engine               = "redis"
  engine_version       = "4.0.10"
  node_type            = "cache.t2.micro"
  num_cache_nodes      = 1
  parameter_group_name = "default.redis4.0"
  port                 = 6379
  subnet_group_name    = "gnar-cache-subnet"
}

resource "aws_subnet" "subnet-a" {
  vpc_id            = "${var.vpc_default_id}"
  availability_zone = "${var.aws_region}a"
  cidr_block        = "${cidrsubnet(data.aws_vpc.default.cidr_block, 4, 3)}"
}

resource "aws_subnet" "subnet-b" {
  vpc_id            = "${var.vpc_default_id}"
  availability_zone = "${var.aws_region}b"
  cidr_block        = "${cidrsubnet(data.aws_vpc.default.cidr_block, 4, 4)}"
}

resource "aws_subnet" "subnet-c" {
  vpc_id            = "${var.vpc_default_id}"
  availability_zone = "${var.aws_region}c"
  cidr_block        = "${cidrsubnet(data.aws_vpc.default.cidr_block, 4, 5)}"
}

resource "aws_elasticache_subnet_group" "gnar-cache-subnet" {
  name       = "gnar-cache-subnet"
  subnet_ids = [
    "${aws_subnet.subnet-a.id}",
    "${aws_subnet.subnet-b.id}",
    "${aws_subnet.subnet-c.id}"
  ]
}

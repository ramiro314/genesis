# https://www.terraform.io/docs/providers/aws/r/s3_bucket.html

resource "aws_s3_bucket" "app" {
  bucket = "${var.s3_app_bucket_name}"
  acl    = "public-read"
}

resource "aws_s3_bucket_object" "logo" {
  bucket = "${var.s3_app_bucket_name}"
  key    = "${var.logo_file_name}"
  source = "./assets/${var.logo_file_name}"
}

resource "aws_s3_bucket" "kops" {
  bucket = "${var.s3_kops_bucket_name}"
}

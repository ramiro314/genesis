# https://www.terraform.io/docs/providers/aws/r/ses_domain_identity.html

resource "aws_ses_domain_identity" "gnar" {
  domain = "${var.domain}"
}

# https://www.terraform.io/docs/providers/aws/r/ses_domain_dkim.html

resource "aws_ses_domain_dkim" "gnar" {
  domain = "${aws_ses_domain_identity.gnar.domain}"
}

data "template_file" "dkim" {
  count    = 3
  template = <<STRING
{
  name: ${element(aws_ses_domain_dkim.gnar.dkim_tokens, count.index)}._domainkey.${var.domain},
  value: ${element(aws_ses_domain_dkim.gnar.dkim_tokens, count.index)}.dkim.amazonses.com
}STRING
}

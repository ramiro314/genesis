variable "aws_access_key" {
  type = "string"
}

variable "aws_secret_key" {
  type = "string"
}

variable "aws_region" {
  type = "string"
}

variable "domain" {
  type = "string"
}

variable "vpc_default_id" {
  type = "string"
}

variable "db_identifier" {
  type = "string"
}

variable "db_username" {
  type = "string"
}

variable "db_password" {
  type = "string"
}

variable "s3_app_bucket_name" {
  type = "string"
}

variable "s3_kops_bucket_name" {
  type = "string"
}

variable "logo_file_name" {
  type = "string"
}

# https://www.terraform.io/docs/providers/aws/r/route53_zone.html

resource "aws_route53_zone" "app" {
  name    = "app.${var.domain}"
}

resource "aws_route53_zone" "cluster" {
  name    = "cluster.${var.domain}"
}

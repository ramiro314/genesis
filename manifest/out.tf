output "iam_kops_key_id" {
  value = "${aws_iam_access_key.kops.id}"
}

output "iam_kops_access_key" {
  value = "${aws_iam_access_key.kops.secret}"
}

output "iam_ses_key_id" {
  value = "${aws_iam_access_key.ses.id}"
}

output "iam_ses_access_key" {
  value = "${aws_iam_access_key.ses.secret}"
}

output "iam_sqs_key_id" {
  value = "${aws_iam_access_key.sqs.id}"
}

output "iam_sqs_access_key" {
  value = "${aws_iam_access_key.sqs.secret}"
}

output "vpc_id" {
  value = "${var.vpc_default_id}"
}

output "kops_network_cidr" {
  value = "${cidrsubnet(data.aws_vpc.default.cidr_block, 4, 6)}"
}

output "db_endpoint" {
  value = "${aws_db_instance.gnar.endpoint}"
}

output "logo_url" {
  value = "${aws_s3_bucket.app.bucket_domain_name}/${aws_s3_bucket_object.logo.key}"
}

output "certificate_arn" {
  value = "${aws_acm_certificate.cert.arn}"
}

output "piste_ecr_uri" {
  value = "${aws_ecr_repository.piste.repository_url}"
}

output "off-piste_ecr_uri" {
  value = "${aws_ecr_repository.off-piste.repository_url}"
}

output "powder_ecr_uri" {
  value = "${aws_ecr_repository.powder.repository_url}"
}

output "app_nameserver_records" {
  value = "${aws_route53_zone.app.name_servers}"
}

output "clutser_nameserver_records" {
  value = "${aws_route53_zone.cluster.name_servers}"
}

output "dkim_cname_records" {
  value = ["${data.template_file.dkim.*.rendered}"]
}

output "ses_txt_record" {
  value = <<STRING
{
  name: _amazonses, value: ${aws_ses_domain_identity.gnar.verification_token}
}STRING
}

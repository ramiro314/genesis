# Project Gnar

## Ops

I used Terraform to provision all relevant AWS resources, except Kubernetes stuff:


- Ensure `terraform` is installed on your box

```
$ brew install version

$ terraform -version
Terraform v0.11.8
```

- Initialize Terraform so it downloads AWS provider.

```
$ terraform init ops/terraform/dev

$ terraform -version
Terraform v0.11.8
+ provider.aws v1.34.0
```

- Modify `bin/terraform.sh` with correct AWS credentials.

- Plan before applying changes.

```
$ ./bin/terraform dev plan
```

- Apply the changes.

```
$ ./bin/terraform dev apply
```
